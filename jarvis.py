import argparse
from task_executor import TaskExecutor

if __name__ == '__main__':
    task_executor = TaskExecutor()
    # execute command using active session token ##############################
    # syntax:
    #   getting password:
    #   jarvis get -n My state street password
    #   jarvis get -n my_state_street_lan
    #   jarvis get -n cloud_password -v 2
    #
    #   setting password:
    #   jarvis set -n my_state_street_lan -p qwerty%af12
    #   jarvis set -n My local pass -p sEcReT PaSsWoRd
    parser = argparse.ArgumentParser(
        prog='jarvis',
        description='Easy, secure, persistent, multi platform password storage')

    subparsers = parser.add_subparsers()

    aut_parser = subparsers.add_parser('login')
    aut_parser.set_defaults(func=task_executor.jarvis_login)

    get_parser = subparsers.add_parser('get')
    get_parser.add_argument('name', nargs='+', help='Unique password name', type=str)
    get_parser.add_argument('-v', '--version', help='Password version. Skip this to get latest version', type=int)
    get_parser.set_defaults(func=task_executor.get_password)

    set_parser = subparsers.add_parser('set')
    set_parser.add_argument('name', nargs='+', help='Unique password name', type=str)
    set_parser.add_argument('-p', '--password', nargs='+', help='Password', type=str, required=True)
    set_parser.set_defaults(func=task_executor.set_password)

    args = parser.parse_args()
    args.func(args)

