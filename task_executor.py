import os.path
import requests
import json
import datetime
import pyperclip
import io

from Crypto.Hash import SHA256
from Crypto.Cipher import AES
from Crypto import Random


class TaskExecutor:
    def __init__(self):
        self.jarvis_login = None
        self.jarvis_password_hash = None
        self.jarvis_session_token = None
        self.jarvis_user_id = None
        # User's home directory. Used to store session
        self.home = os.path.expanduser('~')
        # Full path to session storage directory
        self.session_path = self.home + os.path.normpath('/.jarvis/session')
        # Base URL of API service
        self.api_url = 'http://192.168.11.174:5566/api/v1.0'
        # Maximum password length in this version of client
        self.max_password_length = 510
        # Attachment length value size in bytes
        self.attachment_length_part_size = 2

    def get_password(self, args):
        """ Get password from service, decrypt it and store in copy-paste buffer.

        :param args: Command line arguments from argparse module
        """
        # Check that user's session is valid
        self.check_session()

        # Get password name from input arguments
        password_alias = ' '.join(args.name)

        # Form service call URL
        print('Forming request to API...')
        request = self.api_url + '/users/' + str(self.jarvis_user_id) + '/passwords/' + password_alias
        print('Following API will be called:', request)

        # Make request
        print('Making API request...')
        resp = requests.get(request)
        if resp.status_code == 200:
            print(resp.status_code, '- service response;')

            resp_content = resp.content
            print(resp_content)

            # Get version part out of response
            s_password_version = resp_content[0:2]
            password_version = int(s_password_version)
            print('Received password of version:', password_version)

            s_cp437_decoded_aes_encrypted_password = resp_content[2:len(resp_content)]

            # Convert encrypted password to bytes using MS-DOS cp437 codec
            print('Encoding received password with MS-DOS cp437 codec...')
            b_cp437_encoded_aes_encrypted_password = bytes(s_cp437_decoded_aes_encrypted_password, encoding='cp437')
            print('AES encrypted password encoded with MS-DOS cp437 codec [bytes]:', b_cp437_encoded_aes_encrypted_password)

            # Decrypt password
            print('Decrypting encoded password with AES...')
            aes = AES.new(self.jarvis_password_hash, AES.MODE_CBC, 'This is an IV456')
            b_concatenated_utf8_encoded_password = aes.decrypt(b_cp437_encoded_aes_encrypted_password)
            print('UTF-8 encoded concatenated password [bytes]:', b_concatenated_utf8_encoded_password)

            # Determine attachment length
            print('Determining attachment length...')
            b_utf8_encoded_password_attachment_length = b_concatenated_utf8_encoded_password[
                len(b_concatenated_utf8_encoded_password) - self.attachment_length_part_size:
                len(b_concatenated_utf8_encoded_password)
            ]
            utf8_encoded_password_attachment_length = int.from_bytes(
                b_utf8_encoded_password_attachment_length,
                byteorder='big')
            print('UTF-8 encoded password attachment length [int]:', utf8_encoded_password_attachment_length)

            # Remove attachment from password's body
            print('Removing attachment from UTF-8 encoded password...')
            b_utf8_encoded_password = b_concatenated_utf8_encoded_password[
                0:
                len(b_concatenated_utf8_encoded_password) -
                self.attachment_length_part_size -
                utf8_encoded_password_attachment_length
            ]
            print('UTF-8 encoded password [bytes]:', b_utf8_encoded_password)

            # Decode password bytes to String using utf-8 codec
            print('Decoding password using UTF-8 codec...')
            s_utf8_decoded_password = str(b_utf8_encoded_password, encoding='utf-8')
            print('UTF-8 decoded password [string]:', s_utf8_decoded_password)

            pyperclip.copy(s_utf8_decoded_password)
            print('Your password has been copied to clipboard:', s_utf8_decoded_password)
        else:
            print(resp.status_code, '- service response;', 'Can''t obtain password:', resp.status_code)

    def set_password(self, args):
        """ Encrypt password and store it in backend service.

        :param args: Command line arguments from argparse module
        """
        # Check that user's session is valid
        self.check_session()

        # Get password name from input arguments
        password_alias = ' '.join(args.name)

        # Get password from input arguments
        s_utf8_password = ' '.join(args.password)
        print('Your entered password [string]:', s_utf8_password)

        # Convert to bytes using utf-8 codec
        print('Encoding password using UTF-8 codec...')
        b_utf8_encoded_password = s_utf8_password.encode('utf-8')
        print('UTF-8 encoded password [bytes]:', b_utf8_encoded_password)

        # Section: Complete password to be max length
        # Calculate how many random bytes we need to append to password for it to be exactly max length
        print('Calculating random attachment length...')
        utf8_encoded_password_remain_length = self.max_password_length - len(b_utf8_encoded_password)
        print('UTF-8 encoded password remain length [int]:', utf8_encoded_password_remain_length)

        # Ensure password does not exceed maximum allowed length
        print('Checking password length compliance with max password length...')
        if utf8_encoded_password_remain_length < 0:
            # Throw error. Password must have place for 2 bytes holding number of attached random bytes.
            print('WARN: Your password is too long. Maximum allowed length is:', self.max_password_length)
            print('Try to set shorter password.')
            return
        else:
            print('Your password''s length is acceptable')

        # Generate random attachment for utf8 encoded password
        print('Generating random attachment...')
        rnd_gen = Random.new()
        b_utf8_encoded_password_attachment = rnd_gen.read(utf8_encoded_password_remain_length)
        print('UTF-8 encoded password attachment [bytes]:', b_utf8_encoded_password_attachment)

        # Convert number of appended bytes to 2-byte length attachment
        print('Encoding attachment length into 2 bytes...')
        b_utf8_encoded_password_attachment_length = utf8_encoded_password_remain_length\
            .to_bytes(length=self.attachment_length_part_size, byteorder='big')
        print('UTF-8 encoded password attachment length [bytes]:', b_utf8_encoded_password_attachment_length)

        # Concatenate utd8 encoded password + attachment + attachment length
        print('Concatenating all parts to form message that will be encrypted...')
        b_concatenated_utf8_encoded_password = \
            b_utf8_encoded_password + \
            b_utf8_encoded_password_attachment + \
            b_utf8_encoded_password_attachment_length
        print('Concatenated UTF-8 encoded password [bytes]:', b_concatenated_utf8_encoded_password)

        # Encrypt concatenated max length password with AES
        print('Encrypting message...')
        aes = AES.new(self.jarvis_password_hash, AES.MODE_CBC, 'This is an IV456')
        b_aes_encrypted_password = aes.encrypt(b_concatenated_utf8_encoded_password)
        print('AES encrypted UTF-8 encoded concatenated password [bytes]:', b_aes_encrypted_password)

        # Decode encrypted bytes to String using MS-DOS cp437 codec
        # Because we send encrypted password with JSON. JSON only accepts string for serialization.
        # Database also stores encrypted password as a String.
        print('Decoding encrypted message with MS-DOS cp437 codec...')
        s_cp437_decoded_aes_encrypted_password = str(b_aes_encrypted_password, encoding='cp437')
        print('Encrypted password decoded with MS-DOS cp437 codec [string]:', s_cp437_decoded_aes_encrypted_password)

        # Form service call URL
        print('Forming API request...')
        request = self.api_url + '/users/' + str(self.jarvis_user_id) + '/passwords/' + password_alias
        print('Following API will be called:', request)

        # Make request
        print('Making request to API...')
        byte_stream = io.BytesIO(s_cp437_decoded_aes_encrypted_password)
        resp = requests.post(request, data=byte_stream)
        if resp.status_code == 200:
            password_version = resp.json()['password_version']
            print(resp.status_code, '- service response;', 'Password has been set. Version:', password_version)
        else:
            print(resp.status_code, '- service response;', 'Password has not been set')

    def check_session(self):
        """ Check user's session for validity.

        """
        try:
            # Check session in session storage directory
            with open(self.session_path, 'rb') as session_file:
                session = json.loads(session_file.readline().decode('utf-8'))
                self.jarvis_user_id = session['user_id']
                self.jarvis_session_token = session['token']
                self.jarvis_password_hash = session_file.readline()

                # Check session timestamp. Old session is considered invalid.
                session_timestamp = int(session['timestamp'])
                current_timestamp = int(datetime.datetime.now().timestamp())
                if current_timestamp - session_timestamp > 60 * 15:
                    raise Exception('Session expired')
                else:
                    print('Session is valid and is', current_timestamp - session_timestamp, 'seconds old')

                # TODO: Check current session validity in backend

        except Exception as e:
            # Current user's session is invalid
            print('Failed to obtain session:', e)

            # Ask user for credentials
            jarvis_login, jarvis_password = self.get_credentials()

            # Calculate password's hash
            sha256 = SHA256.new()
            sha256.update(jarvis_password.encode('utf-8'))
            jarvis_password_hash = sha256.digest()
            print('Your password hash is:', jarvis_password_hash)

            self.jarvis_login = jarvis_login
            self.jarvis_password_hash = jarvis_password_hash

            # Get new session
            self.get_session_from_service()

            # Create session storage directory if it does not exist
            try:
                os.mkdir(os.path.join(self.home, '.jarvis'))
            except FileExistsError:
                pass

            # Save session
            with open(self.session_path, 'wb') as session_file:
                session = {
                    'user_id': self.jarvis_user_id,
                    'token': self.jarvis_session_token,
                    'timestamp': int(datetime.datetime.now().timestamp())
                }
                session_file.write((json.dumps(session) + '\n').encode('utf-8'))
                session_file.write(self.jarvis_password_hash)

    @staticmethod
    def get_credentials():
        # Ask user for his login
        jarvis_login = input('Enter your Jarvis login: ')
        # Ask user for his password
        jarvis_password = input('Enter your Jarvis password: ')
        return jarvis_login, jarvis_password

    def get_session_from_service(self):
        """ Get session from backend service.

        """
        # Form service call URL
        request = self.api_url + '/users/?login=' + self.jarvis_login

        # Make request
        resp = requests.get(request)
        if resp.status_code == 200:
            user = resp.json()
            self.jarvis_user_id = user['user_id']
            self.jarvis_session_token = '0000000000000000'
            print('You have been identified with following ID: ', self.jarvis_user_id)
        else:
            print('Unable to check user from backend:', resp.status_code)
            raise Exception('Jarvis Service is unavailable')
